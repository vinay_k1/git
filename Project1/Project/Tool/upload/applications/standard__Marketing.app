<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-Campaign</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Object1__c</tab>
    <tab>Object2__c</tab>
    <tab>Customer_Project__c</tab>
    <tab>Render</tab>
    <tab>R_Meeting__c</tab>
    <tab>Employee__c</tab>
    <tab>DynamicPicklist__c</tab>
    <tab>Student__c</tab>
    <tab>Member__c</tab>
    <tab>Interest__c</tab>
</CustomApplication>
