trigger validation_using_Trigger on Contact(before insert, before update) 
{
 for(Contact acc:trigger.new)
 {
    if(acc.Revenue__c < 2000)
    {
       acc.adderror('Annual revenue cannot be less than 2000');
       acc.Revenue__c.adderror('Annual revenue cannot be less than 2000');
    }
 }
}