trigger HCPREFERENCE on R_Meeting__c (after insert,after update) {
    List<R_Meeting__c> updateTaskList = new List<R_Meeting__c>();
    List<R_Meeting__c> TaskList = new List<R_Meeting__c>();
    Map<Id,Id> updatedMap = new Map<Id,Id>();

    for (R_Meeting__c a: trigger.new)
    {
        //It's much better if apply condition for the expected change in this trigger before adding the Id into the set
     //   updatedMap.put(a.Id, a.Account_oapi__c);
    }

    TaskList = [SELECT Id,Opportunity_Scope__c FROM R_Meeting__c
                WHERE Opportunity_Scope__c IN : updatedMap.keySet()]; 
    for(R_Meeting__c task:TaskList)
    {
        task.Opportunity_Scope__c = updatedMap.get(task.Opportunity_Scope__c); 
        updateTaskList.add(task); 
    }          

    if(!updateTaskList.isEmpty())
    {
        update updateTaskList;
    }    
}