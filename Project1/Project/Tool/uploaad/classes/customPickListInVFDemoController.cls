public class customPickListInVFDemoController {

    public String inputValue { get; set; }

    public PageReference display() {
        return null;
    }

public String selectedCountry1{get;set;}

    public customPickListInVFDemoController(){
    }
     
    public List<SelectOption> getCountriesOptions() {
        List<SelectOption> countryOptions = new List<SelectOption>();
        countryOptions.add(new SelectOption('','-None-'));
        countryOptions.add(new SelectOption('INDIA','India'));
        countryOptions.add(new SelectOption('USA','USA'));
        countryOptions.add(new SelectOption('United Kingdom','UK'));
        
 
        return countryOptions;
    }
    
}