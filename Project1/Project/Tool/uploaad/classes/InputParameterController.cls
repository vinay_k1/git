Public with sharing class InputParameterController {
  Public string myInputParam{get;set;}
  Public string myoutputParam{get;set;}
   
  Public void MyParamMethod(){
   myoutputParam = myInputParam;
  }
}