@isTest
class TvRemoteControlTest
{
@isTest static void testVolumeIncrease()
{
TVRemoteControl rc = new TVRemoteControl(20);
Integer newvolume = rc.increasevolume(15);
System.assertEquals(15,newvolume);
}
@isTest static void testvolumedecrease()
{
TVRemoteControl rc = new TVRemoteControl(20);
Integer newvolume= rc.decreaseVolume(15);
system.assertEquals(15,newVolume);
}
@isTest Static void testvolumedecreaseUndermin()
{
TVRemoteControl rc = new TVRemoteControl(10);
Integer newvolume = rc.decreasevolume(100);
system.assertEquals(100,newVolume);
}
@isTest Static void testGetMenuOptions()
{
String menu = TVRemoteControl.getMenuOptions();
system.assertNotEquals(null,menu);
system.assertNotEquals('',menu);
}
}