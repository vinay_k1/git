// Check a checkbox only when an Opp is changed to Closed Won!
trigger Winning on Opportunity (before update) {
  for (Opportunity opp : Trigger.new) {
    // Access the "old" record by its ID in Trigger.oldMap
    Opportunity oldOpp = Trigger.oldMap.get(opp.Id);

    // Trigger.new records are conveniently the "new" versions!
    Boolean oldOppIsWon = oldOpp.StageName.equals('Closed Won');
    Boolean newOppIsWon = opp.StageName.equals('Closed Won');
    Boolean oldOppIsWon1 = oldOpp.StageName.equals('Prospecting');
    Boolean newOppIsWon1 = opp.StageName.equals('Prospecting');
    
    // Check that the field was changed to the correct value
    if (!oldOppIsWon && newOppIsWon) {
      opp.Active_Customer_Project__c = true;
    }
     if (!oldOppIsWon1 && newOppIsWon1) {
      opp.Active_Customer_Project__c = true;
    }
  }
}