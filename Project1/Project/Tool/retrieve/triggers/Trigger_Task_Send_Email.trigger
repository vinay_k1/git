trigger Trigger_Task_Send_Email on Task (before insert) {
    // Don't forget this- all triggers in SF are bulk triggers and so
    // they can fire on multiple objects. So you need to process objects
    // in a FOR loop.
    for(Task tsk: Trigger.New)
    {
        // Get the OwnerID for this task       
        List<User> owners = [select Name from User where id = :tsk.OwnerId LIMIT 1];
        for(User owner : owners)
        {
            // Check if I am the owner (replace the "MY NAME" with your user name)
            if(owner.Name == 'MY NAME')
            {
                // We will add the send email code here
            }
        }
    }       
   
}