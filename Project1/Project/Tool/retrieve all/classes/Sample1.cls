public class Sample1 
{    
    Set<ID> AcctIds = new Set<ID>();
    public List<Account> Acct {get;set;}
    
    public Sample1() {        
        AcctIds.add('0019000000Ok8Xm');
        AcctIds.add('0019000000leGDC');
               
        String SOQL = 'SELECT Name FROM Account WHERE ID IN : AcctIds';
        Acct = Database.query(SOQL);
    }
}