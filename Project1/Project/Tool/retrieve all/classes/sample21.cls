public class sample21
{ 
    public String Name { get; set; }
   
    public String ownership {get;set;}
      
    public sample21()
    {
        
    }

    public List<SelectOption> getownershipOptions()    
    {    
        List<SelectOption> options =  new List<SelectOption>();    
        options.add(new selectOption('None','--- None ---'));    
        Schema.DescribeFieldResult fieldResult = Employee__c.Name.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }      
}