public class SampleController {
    public Object1__c member {get;set;}
    
    public SampleController() {
        member = new Object1__c();
    }
    
    public void sav(){
        insert member;
        member = new Object1__c();
    }
}