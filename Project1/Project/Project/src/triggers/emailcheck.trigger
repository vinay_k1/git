trigger emailcheck on Employee__c (before update) {
Map<Id,Employee__c> o = new Map<Id,Employee__c>();
o=trigger.oldMap;
for(Employee__c n: trigger.new)
{
Employee__c old = new Employee__c ();
old = o.get(n.id);
if(n.Email__c != old.Email__c)
{
n.Email__c.addError('Email cannot be changed');

}
if(n.Name != old.Name)
{

n.Name.addError('Cannot changed');
}
}

}