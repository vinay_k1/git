trigger updateChildCustomObject on Opportunity (after update){

    List<Object2__c> objectsToUpdate = new List<Object2__c>();
    
    for(Object2__c co :[select Id, Opportunity__c,Opportunity__r.R_Status__c,R_Status__c from Object2__c where 
       Opportunity__c in : trigger.new]){
        co.R_Status__c = co.Opportunity__r.R_Status__c;
        objectsToUpdate.add(co);
    }
    
    if(!objectsToUpdate.isEmpty()){
        update objectsToUpdate;
    }
}