trigger Accownerupdateonfield on Account (Before insert, Before update) {
    Set<Id> setAccowner=new set<Id>();
    for (Account Acc : trigger.New)
    {
    setAccowner.add(Acc.ownerid);
    }
    Map<Id,user> user_map=new map<Id,user>([Select Name from user where id in:setAccowner]);
    for(Account Acc: Trigger.New)
    {
    user usr = user_map.get(Acc.ownerId);
    Acc.sales_Rep__c=usr.Name;
    }
    
}