trigger AccountTrigger on Account (before insert, before update) {
    List<Opportunity> opps = [
        Select Id, Opportunity_Scope__c, Account.Website
        From Opportunity
        Where AccountId In :Trigger.newMap.keySet()
    ];
    for (Opportunity opp : opps) {
        opp.Opportunity_Scope__c = opp.Account.Website;
    }
   // insert opps;
    update opps;
}