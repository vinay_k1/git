trigger Call_AprovalProcess_In_Trigger on Contact (before insert, before update) {
 for(Contact acc:trigger.new){
    if(acc.Revenue__c < 2000){
       approval.ProcessSubmitRequest aprlPrcs = new Approval.ProcessSubmitRequest();      
       aprlPrcs .setComments('Submitting record for approval.');
       aprlPrcs.setObjectId(acc.id);
       approval.ProcessResult result = Approval.process(aprlPrcs);
    }
 }
}