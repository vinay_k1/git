<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Continuous_Integration__c</defaultLandingTab>
    <label>Copado Continuous Integration</label>
    <logo>Copado/Copado_Logo.png</logo>
    <tab>Org__c</tab>
    <tab>Git_Repository__c</tab>
    <tab>Continuous_Integration__c</tab>
    <tab>Deployment__c</tab>
    <tab>Environment__c</tab>
    <tab>Account_Summary</tab>
</CustomApplication>
