public class StudentDetails {

    public StudentDetails(ApexPages.StandardController controller) {

    }

    public list<Student__c> name{get;set;}
    public String val{get;set;}
    public id recId{get;set;}

    public list<SelectOption> getOptions(){
  list<SelectOption> options = new list<SelectOption>();
        options.add(new SelectOption('', '-None-'));
        for(Student__c  s : [Select id, name from Student__c] ){
            options.add(new SelectOption (s.id, s.name));
        }
        return options;
        
    }
    public pageReference showDetails(){
        if(val!=null&& val!=''){
            recId = val;
        }
        else recId=null;
        return null;
    }
    }