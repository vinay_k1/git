trigger triggername on R_Meeting__c (Before insert, Before update) {

set<Id> oppIds=new set<Id>();
Map<Id,Date> oppNewMap=new Map<Id,Date>();
for(R_Meeting__c r:Trigger.New)
{
    oppIds.add(r.Opportunity__c);
}
List<Opportunity> oppList=[select Id,CloseDate from Opportunity where Id IN:oppIds];
for(Opportunity o:oppList)
{
 oppNewMap.put(o.Id,o.CloseDate);   
}
for(R_Meeting__c r:Trigger.New)
{
    if(r.GE_PW_DP_Early_Ship_Limitation__c==0)
    {
        r.GE_PW_DP_Earliest_Ship_Date_A__c=oppNewMap.get(r.Opportunity__c);
    }
}

        List<R_Meeting__c> rmeetList=new List<R_Meeting__c>();
        List<R_Meeting__c> updateRmeetList=new List<R_Meeting__c>();
        Set<ID> oppId=new Set<ID>();
        Map<Id,Opportunity> oppMap= new Map<Id,Opportunity>();
     /*   for(Opportunity opp : NewOppLst) {
            oppMap.put(opp.Id,opp);
        }*/
        oppId=oppMap.keySet();
        rmeetList=[Select id,Opportunity__c,GE_PW_DP_Early_Ship_Limitation__c from R_Meeting__c where Opportunity__c in:oppId];
        for(R_Meeting__c rmeeting : rmeetList)
        {
           Opportunity newOpp = oppMap.get(rmeeting.opportunity__c);
         // if(newOpp.GE_HQ_Business_Tier2__c=='Distributed Power')
          {
          if((rmeeting.GE_PW_DP_Early_Ship_Limitation__c==0))
          {
          rmeeting.GE_PW_DP_Earliest_Ship_Date_A__c=newOpp.CloseDate;
          
           updateRmeetList.add(rmeeting);
          }
          
       //insert updateRmeetList;
          }
         }
         insert updateRmeetList;
       //if(updateRmeetList.size()>0)
       //update  updateRmeetList;
       for(R_Meeting__c member : Trigger.new)   
     {       
         if(member.GE_PW_DP_Required_Delivery_Date__c < member.GE_PW_DP_Earliest_Ship_Date_A__c )     
         {       
             member.GE_DP_Picklist__c = 'Yes';     
         }        
         else    
         {       
             member.GE_DP_Picklist__c = 'No';     
         }        
     }   

     }